<?php

namespace ProjectBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(name="facebook_id", type="string", length=255, nullable=true)
     */
    private $facebook_id;

    /**
     * @ORM\Column(name="facebook_access_token", type="string", length=255, nullable=true)
     */
    private $facebook_access_token;

    /**
     * @ORM\Column(name="firstName", type="string", length=255, nullable=true)
     */
    private $firstName;

    /**
     * @ORM\Column(name="lastName", type="string", length=255, nullable=true)
     */
    private $lastName;

    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return;
    }

    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $status
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    public function getLastName()
    {
        return $this->lastName;
    }

    public function setFacebookId($idFb)
    {
        $this->facebook_id = $idFb;
        return;
    }

    public function getFacebookId()
    {
        return $this->facebook_id;
    }

    public function setFacebookAccessToken($idFb)
    {
        $this->facebook_access_token = $idFb;
        return;
    }

    public function getFacebookAccessToken()
    {
        return $this->facebook_access_token;
    }
}