<?php

namespace ProjectBundle\Controller;

use Facebook\Facebook;
use Facebook\FacebookRequest;
use HWI\Bundle\OAuthBundle\OAuth\ResourceOwner\FacebookResourceOwner;
use ProjectBundle\Form\PostCommentForm;
use ProjectBundle\Form\PostAjaxCommentForm;
use ProjectBundle\Entity\Comment;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class CommentController extends Controller
{
    /**
     * @Route("/comment/create", name="create_comment", options={"expose"=true})
     * @Template
     */
    public function postCommentAction(Request $request)
    {
        $form = $this->createForm(new PostCommentForm());

        $form->handleRequest($request);

        if ($form->isValid()) {
            $data = $form->getData();

            try {
                $commentLogic = $this->get('logic.comment');
                $data['user'] = $this->getUser()->getId();
                $commentLogic->postComment($data);

                $this->get('session')->getFlashBag()->set('messages', 'Комментарий создан');
                return $this->redirectToRoute('index', array(), 301);
            } catch (\Exception $e) {

                $this->get('session')->getFlashBag()->set('errors', 'Увы, не в этот раз +(');
            }
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/comment/{id}", name="get_comment", options={"expose"=true} )
     */
    public function getCommentAction(Request $request, $id)
    {
        $commentLogic = $this->get('logic.comment');
        $comment = $commentLogic->getComment($id);

        return new JsonResponse([
            "comment" => $this->get('serializer')->serialize($comment, 'json')
        ]);
    }

    /**
     * @Route("/comment/edit/{id}", name="put_comment", options={"expose"=true} )
     */
    public function putCommentAction(Request $request, $id)
    {
        $data = $request->request->all();
        $form = $this->createForm(new PostAjaxCommentForm(), new Comment());
        $form->submit($data);

        if($form->isValid()){
            $commentLogic = $this->get('logic.comment');
            $data['user'] = $this->getUser()->getId();
            $data['comment'] = $id;
            $comment = $commentLogic->putComment($data);

            return new JsonResponse([
                "message" => $comment->getMessage()
        ]);
        }

        return new JsonResponse([
            "message" =>  $this->get('serializer')->serialize($form->getErrors(), 'json')
        ]);
    }
}