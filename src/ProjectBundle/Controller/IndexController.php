<?php

namespace ProjectBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class IndexController extends Controller
{
    /**
     * @Route("/", name="index")
     * @Template
     */
    public function indexAction()
    {
        $commentsLogic = $this->get('logic.comment');
        $comments = $commentsLogic->getComments();

        if(!empty($this->getUser()->getFacebookAccessToken()))
        {
            $facebookLogic = $this->get('logic.facebook');
            $friendsList = $facebookLogic->getFriends(20);
;
            return array(
                'comments' => $comments,
                'currentUser' => $this->getUser(),
                'friends' => $friendsList
            );
        }

        return array(
            'comments' => $comments,
            'currentUser' => $this->getUser()
        );
    }
}
