<?php

namespace ProjectBundle\Controller;

use ProjectBundle\Form\StockForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class StockController extends Controller
{
    /**
     * @Route("/stocks", name="stocks")
     * @Template
     */
    public function getStockAction()
    {
        $stockLogic = $this->get('logic.stock');
        $stocks = $stockLogic->getStockByUser($this->getUser()->getId());

        return [
            'stocks' => $stocks
        ];
    }

    /**
     * @Route("/price", name="get_price", options={"expose"=true} )
     */
    public function getPriceAction(Request $request)
    {
        $stockLogic = $this->get('logic.stock');
        $stocks = $stockLogic->getPrice($this->getUser()->getId());

        return new JsonResponse([
            "price" => $stocks
        ]);
    }

    /**
     * @Route("/stock/add", name="post_stock")
     * @Template
     */
    public function setStockAction(Request $request)
    {
        $form = $this->createForm(new StockForm());
        $form->handleRequest($request);

        if($form->isValid()){
            $data = $form->getData();
            $data['user'] = $this->getUser()->getId();

            try{
                $stockLogic = $this->get('logic.stock');
                $stockLogic->postStock($data);

                $this->addFlash(
                    'notice',
                    'Акция добавлена'
                );
            }catch(\Exception $e){
                $this->addFlash(
                    'notice',
                    'Что-то пошло не так +('
                );
            }
        }

        return array(
            'form' =>$form->createView()
        );
    }

    /**
     * @Route("/stock/delete/{id}", name="delete_stock")
     */
    public function deleteStockAction(Request $request, $id)
    {
        try {

            $stockLogic = $this->get('logic.stock');
            $stock = $stockLogic->deleteStock($id, $this->getUser()->getId());

            return $this->redirectToRoute('stocks', array(), 301);
        } catch (\Exception $e) {

            $this->get('session')->getFlashBag()->set('errors', 'Увы, не в этот раз +(');
            return $this->redirectToRoute('stocks', array(), 301);
        }
    }
}