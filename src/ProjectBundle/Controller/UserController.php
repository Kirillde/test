<?php

namespace ProjectBundle\Controller;

use Facebook\Facebook;
use Facebook\FacebookRequest;
use HWI\Bundle\OAuthBundle\OAuth\ResourceOwner\FacebookResourceOwner;
use ProjectBundle\Form\PutUserForm;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

class UserController extends Controller
{
    /**
     * @Route("/user/edit", name="put_user")
     * @Template
     */
    public function putUserAction(Request $request)
    {
        $userLogic = $this->get('logic.user');
        $user = $userLogic->getUser();

        $form = $this->createForm(new PutUserForm());
        $form->handleRequest($request);

        if($form->isValid()){
            $data = $form->getData();
            try{
                $userLogic = $this->get('logic.user');
                $userLogic->putCurrentUser($data);
            }catch(\Exception $e){
                $this->addFlash(
                    'notice',
                    'Что-то пошло не так +('
                );
            }
        }

        return array(
            'user' => $user,
            'form' =>$form->createView()
        );
    }
}
