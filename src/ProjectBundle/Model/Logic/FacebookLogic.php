<?php

namespace ProjectBundle\Model\Logic;
use Facebook\Facebook;

class FacebookLogic
{

    private $fb;

    public function __construct($securityContext, $clientId, $clientSecret, $defaultGraphVersion)
    {
        $this->fb = new Facebook([
            'app_id' => $clientId,
            'app_secret' => $clientSecret,
            'default_graph_version' => $defaultGraphVersion,
            'default_access_token' =>  $securityContext->getToken()->getUser()->getFacebookAccessToken()
        ]);
    }

    public function getFriends($limit)
    {
        $response = $this->fb->get('/me/taggable_friends?field=name&limit='.$limit);
        $friendsList = $response->getGraphEdge()->asArray();

        return $friendsList;
    }
}