<?php

namespace ProjectBundle\Model\Logic;

class UserLogic
{
    private $userEntity;

    public function __construct($userEntity)
    {
        $this->userEntity = $userEntity;
    }

    public function getUser()
    {
        return $this->userEntity->getUser();
    }

    public function putCurrentUser($data)
    {
        return $this->userEntity->putCurrentUser($data);
    }
}