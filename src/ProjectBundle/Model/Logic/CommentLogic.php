<?php

namespace ProjectBundle\Model\Logic;

class CommentLogic
{

    private $commentEntity;

    public function __construct($commentEntity)
    {
        $this->commentEntity = $commentEntity;
    }

    public function getComments()
    {
        return $this->commentEntity->getComments();
    }

    public function postComment($data)
    {
        return $this->commentEntity->postComment($data);
    }

    public function getComment($id)
    {
        return $this->commentEntity->getComment($id);
    }

    public function putComment($data)
    {
        return $this->commentEntity->putComment($data);
    }
}