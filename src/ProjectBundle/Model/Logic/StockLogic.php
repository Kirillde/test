<?php

namespace ProjectBundle\Model\Logic;

use DirkOlbrich\YahooFinanceQuery\YahooFinanceQuery;

class StockLogic
{

    private $stockEntity;

    public function __construct($stockEntity)
    {
        $this->stockEntity = $stockEntity;
    }

    public function postStock($data)
    {
        return $this->stockEntity->postStock($data);
    }

    public function getPrice($id)
    {

        $stocks = $this->getStockByUser($id);

        $query = new YahooFinanceQuery;
        $end = new \DateTime();
        $endDate = clone $end;
        $begin = $end->modify('-2 year');
        $startDate = $begin->format("Y-m-d");
        $finalDate = $endDate->format("Y-m-d");
        $param = 'd';

        $interval = new \DateInterval('P1D');
        $daterange = new \DatePeriod($begin, $interval, $endDate);

        $result = [];
        $c = 0;
        foreach ($stocks as $stock) {
            $data = $query->historicalQuote($stock->getName(), $startDate, $finalDate, $param)->get();

            $count = $stock->getCount();
            $price = 0;
            $i = 0;

            foreach($daterange as $date){
                $result[$c][$i]['price'] = $price;
                $result[$c][$i]['date'] = $date->format("Y-m-d");

                foreach($data as $stock){
                    if ($date->format("Y-m-d") == $stock['Date']) {
                        $result[$c][$i]['price'] = $stock['Close'] * $count;
                        $result[$c][$i]['date'] = $date->format("Y-m-d");
                        $price = $stock['Close']* $count;;
                    }
                }
                $i++;
            }
            $c++;
        }

        $total = [];
        foreach($result as $res){
            for($i=0; $i<count($res); $i++){
                $total[$i]['price'] = (empty($total[$i]['price'])) ? $res[$i]['price'] : $total[$i]['price']+$res[$i]['price'];
                $total[$i]['date'] = $res[$i]['date'];
            }
        }
        return $total;
    }

    public function getStockByUser($id)
    {
        return $this->stockEntity->getStockByUser($id);
    }

    public function deleteStock($id, $user)
    {
        $this->stockEntity->deleteStock($id, $user);
    }
}