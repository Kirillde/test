<?php

namespace ProjectBundle\Model\Entity;

class Entity
{
    public $em;

    public function __construct()
    {

    }

    protected function getData($name)
    {
        return $this->em->getRepository('ProjectBundle:' . ucfirst($name));
    }

    protected function persist($object)
    {
        $this->em->persist($object);
    }

    protected function remove($object)
    {
        $this->em->remove($object);
    }

    protected function flush()
    {
        $this->em->flush();
    }

    protected function beginTransaction()
    {
        $this->em->getConnection()->beginTransaction();
    }

    protected function commit()
    {
        $this->em->getConnection()->commit();
    }

    protected function rollback()
    {
        $conn = $this->em->getConnection();
        $conn->rollback();
        $conn->close();
    }
}
