<?php

namespace ProjectBundle\Model\Entity;

use ProjectBundle\Entity\Comment;
use Doctrine\Common\Collections;

class CommentEntity extends Entity
{

    public function __construct($em)
    {
        $this->em = $em;
    }

    public function getComments()
    {
        return $this->getData('Comment')->findAll();
    }

    public function postComment($data)
    {
        $user = $this->getData('User')->find($data['user']);

        $comment = new Comment();
        $comment
            ->setUser($user)
            ->setMessage($data['message'])
        ;

        $this->persist($comment);
        $this->flush();
    }

    public function getComment($id)
    {
        return $this->getData('Comment')->find($id);
    }

    public function putComment($data)
    {
        $comment = $this->getData('Comment')->find($data['comment']);

        $comment
            ->setMessage($data['message'])
        ;

        $this->persist($comment);
        $this->flush();

        return $comment;
    }

}