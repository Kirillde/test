<?php

namespace ProjectBundle\Model\Entity;

use ProjectBundle\Entity\Stock;
use Doctrine\Common\Collections;

class StockEntity extends Entity
{

    public function __construct($em)
    {
        $this->em = $em;
    }

    public function postStock($data)
    {
        $user = $this->getData('User')->find($data['user']);

        $stock = new Stock();
        $stock
            ->setName($data['name'])
            ->setUser($user)
            ->setCount($data['count'])
        ;

        $this->persist($stock);
        $this->flush();

        return $stock;
    }

    public function getStockByUser($id)
    {
        $stock = $this->getData('Stock')->findBy(['user' => $id]);

        return $stock;
    }

    public function deleteStock($id, $user)
    {
        $stock = $this->getData('Stock')->findOneBy(['id' => $id, 'user' => $user]);

        $this->remove($stock);
        $this->flush();
    }
}