<?php

namespace ProjectBundle\Model\Entity;

class UserEntity extends Entity
{
    private $user;

    public function __construct($em, $securityContext)
    {
        $this->em = $em;
        $this->user = $securityContext->getToken()->getUser();
    }

    public function getUser()
    {
        return $this->user;
    }

    public function putCurrentUser($data)
    {
        $this->user
            ->setLastName($data['lastName'])
            ->setFirstName($data['firstName'])
        ;

        $this->persist($this->user);
        $this->flush();

        return $this->user;
    }
}