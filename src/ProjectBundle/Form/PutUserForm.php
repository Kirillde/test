<?php

namespace ProjectBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class PutUserForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'firstName',
            'text',
            array(
                'label' => 'Имя',
                'constraints' => array(
                   // new NotBlank(),
                    new Length(
                        array(
                            'max' => 255,
                            'maxMessage' => 'Максимальная длина - 255 символа'
                        ))
                ),
            )
        );

        $builder->add(
            'lastName',
            'text',
            array(
                'label' => 'Фамилия',
                'constraints' => array(
                   // new NotBlank(),
                    new Length(
                        array(
                            'max' => 255,
                            'maxMessage' => 'Максимальная длина - 255 символа'
                        ))
                ),
            )
        );

        $builder->add(
            'submit',
            'submit',
            array(
                'label' => 'Отправить',
                'attr' => array(
                    'class' => 'submit'
                )
            )
        );
    }

    public function getName()
    {
        return 'User';
    }

}