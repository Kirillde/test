<?php

namespace ProjectBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PostAjaxCommentForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'message',
            'textarea',
            array(
                'label' => 'Комментарий',
                'constraints' => array(
                    new NotBlank(),
                    new Length(
                        array(
                            'max' => 2048,
                            'maxMessage' => 'Максимальная длина - 2048 символа'
                        ))
                ),
            )
        );

        $builder->add(
            'submit',
            'submit',
            array(
                'label' => 'Отправить',
                'attr' => array(
                    'class' => 'submit'
                )
            )
        );
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ProjectBundle\Entity\Comment',
            'csrf_protection' => false
        ));
    }

    public function getName()
    {
        return 'Comment';
    }

}