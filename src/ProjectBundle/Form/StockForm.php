<?php

namespace ProjectBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class StockForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'name',
            'text',
            array(
                'label' => 'Название',
                'constraints' => array(
                    new NotBlank(),
                    new Length(
                        array(
                            'max' => 255,
                            'maxMessage' => 'Максимальная длина - 15 символа'
                        ))
                ),
            )
        );

        $builder->add(
            'count',
            'integer',
            array(
                'label' => 'Количество',
                'constraints' => array(
                    new NotBlank(),
                    new Length(
                        array(
                            'max' => 15,
                            'maxMessage' => 'Максимальная длина - 15 символов'
                        ))
                ),
            )
        );

        $builder->add(
            'submit',
            'submit',
            array(
                'label' => 'Добавить',
                'attr' => array(
                    'class' => 'submit'
                )
            )
        );
    }

    public function getName()
    {
        return 'Stock';
    }

}